<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Movies */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Movies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movies-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <table cellspacing="5" cellpadding="10"  width="100%">
        <tr>
            <td width="20%">
                <?= Html::img($model->picture, ['height'=>'100%']) ?>
            </td>
            <td valign="middle" width="80%">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
//            'id',
                        'title',
//            'picture',
//                        [
//                            'attribute'=>'picture',
//                            'value'=>$model->picture,
//                            'format' => ['image',['width'=>'100','height'=>'100']],
//                        ],
                        [
                            'label' => 'Жанры',
                            'attribute' => 'genre',
                            'value' => function($model) {
                                return implode(', ', ArrayHelper::getColumn($model->genres, 'genre'));
                            }
                        ],
                        'baseimage',
                    ],
                ]) ?>
            </td>
        </tr>
    </table>

<!--    <img src="smiley.gif" alt="Smiley face" height="42" width="42">-->



</div>
