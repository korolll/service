<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Genres;
use budyaga\cropper\Widget;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Movies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movies-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'picture')->widget(Widget::className(), [
        'uploadUrl' => Url::toRoute('/movies/uploadPhoto'),
    ]) ?>

    <?= $form->field($model, 'genre_ids')->checkboxList(ArrayHelper::map(Genres::find()->all(),'id','genre')); ?>

    <?= $form->field($model, 'baseimage')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
