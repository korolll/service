<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use backend\models\MoviesGenres;
use backend\models\Movies;
use yii\helpers\Url;
use yii\bootstrap\Button;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MoviesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Movies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movies-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Movies', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'format' => 'raw',
                'value' => function($data){
                    return Html::img(Url::toRoute($data->picture),[
                        'alt'=>'yii2 - картинка в gridview',
                        'style' => 'width:100px;'
                    ]);
                },

            ],
            'title',
            [
                    'label' => 'Жанры',
                    'attribute' => 'genre',
                    'value' => function($model) {
                        return implode(', ', ArrayHelper::getColumn($model->genres, 'genre'));
                }
            ],

            [
                    'label' => 'Главная',
                    'format' => 'raw',
                    'value' => function($data){
                            return Button::widget([
                                'label' => 'Primary',
                                'options' => ['class' => $data->baseimage ? 'btn btn-success' : 'btn'],
                                'id'=>$data->id,
                            ]);
                    }

            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>


<?php
$js = <<<JS
$('table .btn').on('click', function(){
    var btn = $(this);
    $.post('change', {id : $(this).attr('id')}, function () {
        $('table .btn').removeClass('btn-success');
        btn.addClass("btn-success");       
    });
    return false;
});
JS;
$this->registerJs($js);
?>