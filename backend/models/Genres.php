<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "genres".
 *
 * @property int $id
 * @property string $genre
 */
class Genres extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'genres';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'genre'], 'required'],
            [['id'], 'integer'],
            [['genre'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'genre' => 'Genre',
        ];
    }
}
