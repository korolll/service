<?php

namespace backend\models;

use Yii;



/**
 * This is the model class for table "movies".
 *
 * @property int $id
 * @property string $title
 * @property string $picture
 * @property int $baseimage
 *
 * @property MoviesGenres[] $moviesGenres
 * @property Genres[] $genres
 */
class Movies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movies';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'genre_ids' => 'genres',
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'picture'], 'required'],
            [['baseimage'], 'integer'],
            [['title', 'picture'], 'string', 'max' => 255],
            [['genre_ids'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'picture' => 'Картинка',
            'baseimage' => 'На главный экран',
        ];
    }
//    public function beforeSave($model)
//    {
//        if (!parent::beforeSave($model)) {
//            return false;
//        }
//        if ($_POST['Movies']['baseimage'] == 1){
//            Movies::updateAll(['baseimage' => 0],['=', 'baseimage', 1]);
//        }
//        return true;
//    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoviesGenres()
    {
        return $this->hasMany(MoviesGenres::className(), ['movie_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGenres()
    {
        return $this->hasMany(Genres::className(), ['id' => 'genre_id'])->viaTable('movies_genres', ['movie_id' => 'id']);
    }
}
