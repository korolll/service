<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RedisController extends Controller
{
    public function actionStart()
    {

        $i=0;
        while ($i<=0):

        Yii::$app->redis->rpush('events_1', json_encode([
            'id'=>$i,
            'status'=>3,
            'action_id'=>rand(1,7),
            'minutes'=>rand(0,60),
            'seconds'=>rand(0,60),
            'period'=>rand(1,3),
            'sportsman_id'=>rand(1,16),
        ]));

        Yii::$app->redis->rpush('online_users', json_encode([
            'user_id' => 2,
            'instance_id' => 1,
            'status' => 1,
            'game_id' => 1,
        ]));

        $i++;
//        sleep(rand(1,3));
        endwhile;
        }
}