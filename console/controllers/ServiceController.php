<?php

namespace console\controllers;

use console\components\AchievementsManager;
use console\components\CalculateGame;
use console\components\UsersManager;
use console\components\ServiceManager;
use yii\console\Controller;


class ServiceController extends Controller
{
    /**
     * @inheritdoc
     */
    public $game_id = 1;
    public $events_pos = 0;
    public $users_pos = 0;

    /**
     * Start Service
     */
    public function actionStart()
    {
        /** @var CalculateGame $game */
        $game = new CalculateGame();
        /** @var UsersManager $users */
        $users = new UsersManager();
        /** @var ServiceManager $events */
        $service = new ServiceManager($this->game_id);


        while (count($service->real_games) != 0) :
            $event_queue = $service->newEvents($this->events_pos);
            if ($event_queue) {
                $game->scanGame($event_queue);
                $service->sendTop($game->score, $users);

                $user_queue = $service->newUsers($this->users_pos);
                $online_list = $user_queue != null ? $users->getOnlineUser($user_queue) : $users->online;
                $service->sendOnline($users, $online_list, $game->score);
            }
            sleep(1);
        endwhile;

        $this->calculateAchievements();

    }

    public function calculateAchievements()
    {
        $achievements = new AchievementsManager($this->game_id);
        $achievements->calculateAll();
    }











}
