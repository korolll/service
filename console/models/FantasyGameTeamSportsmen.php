<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "fantasy_game__team_sportsmen".
 *
 * @property string $team_id
 * @property string $sportsman_id
 * @property string $role_id
 *
 * @property FantasyGameTeams $team
 * @property Sportsmen $sportsman
 */
class FantasyGameTeamSportsmen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fantasy_game__team_sportsmen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'sportsman_id', 'role_id'], 'required'],
            [['team_id', 'sportsman_id', 'role_id'], 'integer'],
            [['team_id', 'sportsman_id'], 'unique', 'targetAttribute' => ['team_id', 'sportsman_id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => FantasyGameTeams::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['sportsman_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sportsmen::className(), 'targetAttribute' => ['sportsman_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'sportsman_id' => 'Sportsman ID',
            'role_id' => 'Role ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(FantasyGameTeams::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportsman()
    {
        return $this->hasOne(Sportsmen::className(), ['id' => 'sportsman_id']);
    }
}
