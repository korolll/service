<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "fantasy_real_games".
 *
 * @property string $fantasy_game_id
 * @property string $real_game_id
 *
 * @property FantasyGames $fantasyGame
 * @property RealGames $realGame
 */
class FantasyRealGames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fantasy_real_games';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fantasy_game_id', 'real_game_id'], 'required'],
            [['fantasy_game_id', 'real_game_id'], 'integer'],
            [['fantasy_game_id', 'real_game_id'], 'unique', 'targetAttribute' => ['fantasy_game_id', 'real_game_id']],
            [['fantasy_game_id'], 'exist', 'skipOnError' => true, 'targetClass' => FantasyGames::className(), 'targetAttribute' => ['fantasy_game_id' => 'id']],
            [['real_game_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealGames::className(), 'targetAttribute' => ['real_game_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fantasy_game_id' => 'Fantasy Game ID',
            'real_game_id' => 'Real Game ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFantasyGame()
    {
        return $this->hasOne(FantasyGames::className(), ['id' => 'fantasy_game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealGame()
    {
        return $this->hasOne(RealGames::className(), ['id' => 'real_game_id']);
    }
}
