<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "real_game__teams".
 *
 * @property string $game_id
 * @property string $team_id
 * @property int $is_host
 * @property int $score
 * @property int $is_winner
 *
 * @property RealGames $game
 * @property RealTeams $team
 */
class RealGameTeams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'real_game__teams';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'team_id'], 'required'],
            [['game_id', 'team_id', 'is_host', 'score', 'is_winner'], 'integer'],
            [['game_id', 'team_id'], 'unique', 'targetAttribute' => ['game_id', 'team_id']],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealGames::className(), 'targetAttribute' => ['game_id' => 'id']],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => RealTeams::className(), 'targetAttribute' => ['team_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'game_id' => 'Game ID',
            'team_id' => 'Team ID',
            'is_host' => 'Is Host',
            'score' => 'Score',
            'is_winner' => 'Is Winner',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(RealGames::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(RealTeams::className(), ['id' => 'team_id']);
    }
}
