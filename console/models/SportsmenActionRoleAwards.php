<?php

namespace console\models;


/**
 * This is the model class for table "sportsmen__action_role_awards".
 *
 * @property string $action_id
 * @property string $role_id
 * @property string $award
 *
 * @property SportsmenActions $action
 * @property SportsmenRoleTypes $role
 */
class SportsmenActionRoleAwards extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sportsmen__action_role_awards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action_id', 'role_id'], 'required'],
            [['action_id', 'role_id'], 'integer'],
            [['award'], 'number'],
            [['action_id', 'role_id'], 'unique', 'targetAttribute' => ['action_id', 'role_id']],
            [['action_id'], 'exist', 'skipOnError' => true, 'targetClass' => SportsmenActions::className(), 'targetAttribute' => ['action_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => SportsmenRoleTypes::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'action_id' => 'Action ID',
            'role_id' => 'Role ID',
            'award' => 'Award',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(SportsmenActions::className(), ['id' => 'action_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(SportsmenRoleTypes::className(), ['id' => 'role_id']);
    }
}
