<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "users_achievement".
 *
 * @property string $id
 * @property string $user_id
 * @property string $achievement_id
 * @property string $number_actions
 *
 * @property Achievements $achievement
 */
class UsersAchievement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_achievement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'achievement_id', 'number_actions'], 'required'],
            [['user_id', 'achievement_id', 'number_actions'], 'integer'],
            [['achievement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Achievements::className(), 'targetAttribute' => ['achievement_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'achievement_id' => 'Achievement ID',
            'number_actions' => 'Number Actions',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAchievement()
    {
        return $this->hasOne(Achievements::className(), ['id' => 'achievement_id']);
    }
}
