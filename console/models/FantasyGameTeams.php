<?php

namespace console\models;


/**
 * This is the model class for table "fantasy_game__teams".
 *
 * @property string $id
 * @property string $user_id
 * @property string $game_id
 * @property string $score
 * @property int $place
 * @property string $award
 *
 * @property FantasyGameTeamSportsmen[] $fantasyGameTeamSportsmens
 * @property Sportsmen[] $sportsmen
 * @property Users $user
 * @property FantasyGames $game
 */
class FantasyGameTeams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fantasy_game__teams';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'game_id'], 'required'],
            [['user_id', 'game_id', 'place', 'award'], 'integer'],
            [['score'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => FantasyGames::className(), 'targetAttribute' => ['game_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'game_id' => 'Game ID',
            'score' => 'Score',
            'place' => 'Place',
            'award' => 'Award',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFantasyGameTeamSportsmens()
    {
        return $this->hasMany(FantasyGameTeamSportsmen::className(), ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportsmen()
    {
        return $this->hasMany(Sportsmen::className(), ['id' => 'sportsman_id'])->viaTable('fantasy_game__team_sportsmen', ['team_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(FantasyGames::className(), ['id' => 'game_id']);
    }
}
