<?php
/**
 * Created by PhpStorm.
 * User: Д&К
 * Date: 08.05.2018
 * Time: 18:00
 */

namespace console\components;

use console\models\FantasyGameTeamSportsmen;
use console\models\SportsmenActionRoleAwards;



class CalculateGame
{
    public $team_sportsman;
    public $role_awards;
    public $data;
    public $score = [];

    /**
     * CalculateGame constructor.
     */
    public function __construct()
    {
        $this->role_awards = $this->loadRoleAwards();
        $this->team_sportsman = $this->loadTeamSportsmen();
    }

    /**
     * @return FantasyGameTeamSportsmen[]
     */
    public function loadTeamSportsmen()
    {
        return FantasyGameTeamSportsmen::find()->all();
    }

    /**
     * @return SportsmenActionRoleAwards[]
     */
    public function loadRoleAwards()
    {
        return SportsmenActionRoleAwards::find()->all();
    }

    /**
     * Scan new events, calculate teams score list
     *
     * @param array $events
     *
     */
    public function scanGame($events)
    {
        foreach ($events as $event) {
            $this->data = json_decode($event, true);
            $this->scanSportsman($this->data['sportsman_id']);
        }
    }

    /**
     * Scan for 1 event FantasyGameTeamSportsmen by sportsman id, generate awards['team_id', 'score']
     *
     * @param integer $id
     *
     */
    public function scanSportsman($id)
    {
        foreach ($this->team_sportsman as $sportsman) {
            if ($sportsman->sportsman_id == $id) {
                $award = $this->getAward($sportsman);
                $team_id = $sportsman->team_id;
                $this->countScore($team_id, $award);
            }
        }
    }

    /**
     * Find award by sportsman, role & action
     *
     * @param FantasyGameTeamSportsmen $sportsman
     * @return float|null
     */
    public function getAward($sportsman)
    {
        $action_id = $this->data['action_id'];
        foreach ($this->role_awards as $role_award) {
            if ($role_award->role_id == $sportsman->role_id & $role_award->action_id == $action_id) {
                return round($role_award->award);
            }
        }
        return null;
    }


    /**
     * Push|calculate score, sort
     *
     * @param integer $team_id
     * @param integer $award
     *
     */
    public function countScore($team_id, $award)
    {
        isset($this->score[$team_id]) ? $this->score[$team_id] += $award : $this->score[$team_id] = $award;
        uasort($this->score, function ($a, $b){
            if ($a == $b) {
                return 0;
            }
            return ($a > $b) ? -1 : 1;
        });
    }


}