<?php

namespace console\components;

use Yii;
use app\models\UsersAchievement;

/**
 * This is the model class for table "users_experience".
 *
 * @property int $id
 * @property int $user_id
 * @property int $experience
 *
 * @property UsersAchievement[] $usersAchievements
 */
class UsersExperience extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'experience'], 'required'],
            [['user_id', 'experience'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'experience' => 'Experience',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersAchievements()
    {
        return $this->hasMany(UsersAchievement::className(), ['user_id' => 'user_id']);
    }
}
