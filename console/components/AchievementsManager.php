<?php
/**
 * Created by PhpStorm.
 * User: Д&К
 * Date: 15.05.2018
 * Time: 16:26
 */

namespace console\components;


use console\models\FantasyGameTeams;
use app\models\UsersAchievement;
use console\models\AchievementAwards;

class AchievementsManager
{
    public $fantasy_game_id;
    public $users_for_update = [];

    public function __construct($id)
    {
        $this->fantasy_game_id = $id;
    }

    public function calculateAll()
    {
        $this->calcWinnersAchieve();
        $this->calcParticipateTenGames();
        $this->calcOneHundredTokens();
        $this->calculateExperience();
    }

    /**
     * Win 1 football fantasy game
     */
    public function calcWinnersAchieve()
    {
        $achievement_id = 1;
        /** @var FantasyGameTeams $winner */
        $winner = FantasyGameTeams::find()->where(['game_id' => $this->fantasy_game_id])->orderBy(['place' => SORT_DESC])->one();
        $user_id = $winner->user_id;
        $user = UsersAchievement::findOne(['user_id' => $user_id, 'achievement_id' => $achievement_id]);
        if ($user) {
            $user->number_actions = FantasyGameTeams::find()->where(['user_id' => $user_id, 'place' => 1])->count();
            $user->save();
        } else {
            $achieve = new UsersAchievement();
            $achieve->user_id = $user_id;
            $achieve->achievement_id = $achievement_id;
            $achieve->number_actions = 1;
            $achieve->save();
        }
        if (!in_array($user_id, $this->users_for_update)) $this->users_for_update[] = $user_id;
    }

    /**
     * Participate in 10 football fantasy game
     */
    public function calcParticipateTenGames()
    {
        $achievement_id = 2;
        $participants = FantasyGameTeams::find()->where(['game_id' => $this->fantasy_game_id])->all();
        /** @var FantasyGameTeams $participant */
        foreach ($participants as $participant) {
            $user_id = $participant->user_id;
            $user = UsersAchievement::findOne(['user_id' => $user_id, 'achievement_id' => $achievement_id]);
            if ($user) {
                $user->number_actions = FantasyGameTeams::find()->where(['user_id' => $user_id])->count();
                $user->save();
            } else {
                $achieve = new UsersAchievement();
                $achieve->user_id = $user_id;
                $achieve->achievement_id = $achievement_id;
                $achieve->number_actions = 1;
                $achieve->save();
            }
            if (!in_array($user_id, $this->users_for_update)) $this->users_for_update[] = $user_id;
        }
    }

    /**
     * Be in the top-3 winners in football game with more than 1000 players
     */
    public function calcTopThreeOfThousand()
    {
        $achievement_id = 3;
        if (FantasyGameTeams::find()->where(['game_id' => $this->fantasy_game_id])->count() > 1000) {
            $users = $this->getTopThree();
            foreach ($users as $user_id) {
                $user = UsersAchievement::findOne(['user_id' => $user_id, 'achievement_id' => $achievement_id]);
                if ($user) {
                    $user->number_actions = FantasyGameTeams::find()->where(['user_id' => $user_id])->count();
                    $user->save();
                } else {
                    $achieve = new UsersAchievement();
                    $achieve->user_id = $user_id;
                    $achieve->achievement_id = $achievement_id;
                    $achieve->number_actions = 1;
                    $achieve->save();
                }
                if (!in_array($user_id, $this->users_for_update)) $this->users_for_update[] = $user_id;
            }
        }
    }

    /**
     * Return top UsersManager for calculation TopThreeOfThousand
     *
     * @return array
     */
    public function getTopThree()
    {
        $users_id = [];
        $first_place = FantasyGameTeams::find()->where(['game_id' => $this->fantasy_game_id, 'place' => 1])->all();
        /** @var FantasyGameTeams $data */
        foreach ($first_place as $data) {
            $users_id[] = $data->user_id;
        }
        if (count($users_id) >= 3) return $users_id;

        $second_place = FantasyGameTeams::find()->where(['game_id' => $this->fantasy_game_id, 'place' => 2])->all();
        foreach ($second_place as $data) {
            $users_id[] = $data->user_id;
        }
        if (count($users_id) >= 3) return $users_id;

        $third_place = FantasyGameTeams::find()->where(['game_id' => $this->fantasy_game_id, 'place' => 3])->all();
        foreach ($third_place as $data) {
            $users_id[] = $data->user_id;
        }
        return $users_id;
    }

    /**
     * Win more than 100 tokens in football fantasy game
     */
    public function calcOneHundredTokens()
    {
        $achievement_id = 4;
        $winners = FantasyGameTeams::find()->where(['game_id' => $this->fantasy_game_id])->where('award >= 100')->all();
        /** @var FantasyGameTeams $user */
        foreach ($winners as $user) {
            $user = UsersAchievement::findOne(['user_id' => $user->user_id, 'achievement_id' => $achievement_id]);
            switch (true) {
                case ($user->award >= 100 && $user->award < 300) : $level = 1; break;
                case ($user->award >= 300 && $user->award < 500) : $level = 2; break;
                case ($user->award >= 500) : $level = 3;
            }
            if ($user) {
//                $level = $this->
            } else {
                $achieve = new UsersAchievement();
                $achieve->user_id = $user->user_id;
                $achieve->achievement_id = $achievement_id;
                $achieve->number_actions = 1;
                $achieve->save();
            }
            // TODO
        }
    }


    /**
     * Calculate Experience for users
     */
    public function calculateExperience()
    {
        foreach ($this->users_for_update as $user_id) {
            /** @var integer $experience */
            $experience = 0;
            /** @var UsersAchievement $user_achievements */
            $user_achievements = UsersAchievement::find()->where(['user_id' => $user_id])->all();
            /** @var UsersAchievement $achievement */
            foreach ($user_achievements as $achievement) {
                /** @var AchievementAwards[] $achievements */
                $achievements = AchievementAwards::find()
                    ->where(['id' => $achievement->achievement_id])
                    ->orderBy(['number_actions' => SORT_ASC])
                    ->all();
                if ($achievement->number_actions >= $achievements[0]->number_actions && $achievement->number_actions < $achievements[1]->number_actions)
                    $experience += $achievements[0]->award;
                if ($achievement->number_actions >= $achievements[1]->number_actions && $achievement->number_actions < $achievements[2]->number_actions)
                    $experience += $achievements[1]->award;
                if ($achievement->number_actions >= $achievements[2]->number_actions)
                    $experience += $achievements[2]->award;
            }
        }
    }


}