<?php
/**
 * Created by PhpStorm.
 * User: Д&К
 * Date: 08.05.2018
 * Time: 18:00
 */

namespace console\components;

use console\models\FantasyGameTeams;
use common\models\User;


class UsersManager
{

    public $online = array();
    public $gameTeams;
    public $users;

    public function __construct()
    {
        $this->gameTeams = $this->loadGameTeams();
        $this->users = $this->loadUsers();
    }

    /**
     * @return FantasyGameTeams[]
     */
    public function loadGameTeams()
    {
        return FantasyGameTeams::find()->all();
    }

    /**
     * @return UsersManager[]
     */
    public function loadUsers()
    {
        return User::find()->all();
    }


    /**
     * Scan new messages and add|remove user from $online[]
     *
     * @param $users
     * @return array
     */
    public function getOnlineUser($users)
    {
        foreach ($users as $data) {
            $user = json_decode($data, true);
            $user['status'] == 1 ? $this->addOnlineUser($user) : $this->removeOnlineUser($user);
        }
        return $this->online;
    }


    /**
     * Add $user to $online[]
     *
     * @param $user
     */
    public function addOnlineUser($user)
    {
        $this->online[] = $user;
    }


    /**
     * Remove $user from $online[]
     *
     * @param $user
     */
    public function removeOnlineUser($user)
    {
        $index = $this->getUserIndex($user['user_id']);
        unset($this->online[$index]);
    }


    /**
     * Scan & find user index in $online[] for unset
     *
     * @param $user_id
     * @return int|null
     */
    public function getUserIndex($user_id)
    {
        $index = null;
        if ($this->online) $index = array_search($user_id, $this->online);
        return $index;
    }


}