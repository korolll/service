<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'nodeServer' => [
        1,
        2,
        3,
        4,
        5
    ]
];
