<?php

namespace api\controllers;

use console\components\AchievementsManager;
use console\components\CalculateGame;
use console\models\FantasyRealGames;
use console\models\Users;
use console\components\UsersManager;
use Yii;
use yii\helpers\ArrayHelper;
use yii\console\Controller;


class ServiceController extends Controller
{
    /**
     * @inheritdoc
     */

    public $game_id = 1;
    public $real_games = [];
    public $events_pos = 0;
    public $users_pos = 0;

    /**
     *
     */

    public function actionStart()
    {
        /** @var CalculateGame $game */
        $game = new CalculateGame();
        /** @var UsersManager $users */
        $users = new UsersManager();
        /** @var \console\models\FantasyRealGames $real_games */
        $this->loadRealGames();

        while (count($this->real_games) != 0) :
        $event_queue = $this->newEvents($this->events_pos);
        if ($event_queue) {
            $game->scanGame($event_queue);
            $top = $this->sendTop($game->score, $users);
            var_dump($top);

            $user_queue = $this->newUsers($this->users_pos);
            $online_list = $user_queue != null ? $users->getOnlineUser($user_queue) : $users->online;
            $this->sendOnline($users, $online_list, $game->score);
        }
            sleep(10);
        endwhile;

        $this->calculateAchievements();

    }

    public function calculateAchievements()
    {
        $achievements = new AchievementsManager($this->game_id);
        $achievements->fantasy_game_id = $this->game_id;
        $achievements->calculateAll();
    }

    public function loadRealGames()
    {
        /** @var FantasyRealGames $real_games */
        $real_games = FantasyRealGames::find()->where(['fantasy_game_id' => $this->game_id])->all();
        foreach ($real_games as $data){
            $this->real_games[] = $data->real_game_id;
        }
    }

    /**
     * Get events, unset realg
     *
     * @param integer $pos
     * @return null|$events
     */
    public function newEvents($pos)
    {
        $events = null;
        foreach ($this->real_games as $real_game){
            $range = Yii::$app->redis->lrange('events_' . $real_game, $pos, -1);
            if (isset($range)){
                foreach ($range as $row){
                    if (isset(json_decode($row, true)['status'])) {
                        $this->removeGame($real_game);
                        break;
                    }
                    $events[] = $row;
                }
            }
        }

        if (!$events) return null;
        $this->events_pos += count($events);
        return $events;
    }



    public function removeGame($id)
    {
        unset($this->real_games[array_search($id, $this->real_games)]);
    }

    /**
     * Extract new messages $users from Redis
     *
     * @param integer $pos
     * @return null|$users
     */
    public function newUsers($pos)
    {
        $users = Yii::$app->redis->lrange('online_users', $pos, -1);
        if (!$users) return null;
        $this->users_pos += count($users);
        return $users;
    }

    /**
     * Generate $top[] & send to Redis servers (from params)
     *
     * @param $score []
     * @param $users []
     * @return array
     */
    public function sendTop($score, $users)
    {
        $place = 1;
        $top = [];

        $user_map = ArrayHelper::map($users->gameTeams, 'id', 'user_id');
        foreach ($score as $team_id => $team_score) {
            $user_id = $user_map[$team_id];
            /** @var Users $user */
            $user = $this->getUser($users, $user_id);
            $message = [
                'team_id' => $team_id,
                'place' => $place,
                'score' => $team_score,
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'city' => $user->city
                ]
            ];
            $top[] = $message;
            $place++;
            if ($place > 5 or $place > count($score) + 1) break;
        }

        if ($top) {
            foreach (Yii::$app->params['nodeServer'] as $server) {
                Yii::$app->redis->rpush('nodejs_' . $server, json_encode($top));
            }
        }

        return $top;
    }


    /**
     * Generate massages for online users & send to Redis servers
     *
     * @param $users []
     * @param $online_list []
     * @param $score []
     */
    public function sendOnline($users, $online_list, $score)
    {
        $user_map = ArrayHelper::map($users->gameTeams, 'user_id', 'id');
        foreach ($online_list as $online) {
            $instance_id = $online['instance_id'];
            $team_id = $user_map[$online['user_id']];
            $place = $this->getPlace($score, $team_id);
            /** @var Users $user */
            $user = $this->getUser($users, $online['user_id']);
            $message = [
                'team_id' => $team_id,
                'place' => $place,
                'score' => isset($score[$team_id]) ? $score[$team_id] : null,
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'city' => $user->city
                ]
            ];
            if (isset($score[$team_id])) Yii::$app->redis->rpush('nodejs_' . $instance_id, json_encode($message));
        }
    }

    /**
     * Count & return Place
     *
     * @param $score
     * @param $id
     * @return int
     */
    public function getPlace($score, $id)
    {
        $place = 1;
        foreach ($score as $team_id => $team_score) {
            if ($id == $team_id) break;
            $place++;
        }
        return $place;
    }

    /**
     * Get User by ID
     *
     * @param $users
     * @param $id
     * @return mixed
     */
    public function getUser($users, $id)
    {
        return $users->users[$id];
    }

}
