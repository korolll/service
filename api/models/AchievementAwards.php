<?php

namespace app\models;


/**
 * This is the model class for table "achievement_awards".
 *
 * @property string $id
 * @property string $level
 * @property string $number_actions
 * @property int $award
 *
 * @property Achievements $id0
 */
class AchievementAwards extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'achievement_awards';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'level', 'number_actions', 'award'], 'required'],
            [['id', 'level', 'number_actions', 'award'], 'integer'],
            [['id', 'level'], 'unique', 'targetAttribute' => ['id', 'level']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Achievements::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'number_actions' => 'Number Actions',
            'award' => 'Award',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Achievements::className(), ['id' => 'id']);
    }
}
